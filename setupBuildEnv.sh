#!/bin/bash
export PATH=$PATH:/opt/brcm/hndtools-mipsel-linux/bin:/opt/brcm/hndtools-mipsel-uclibc/bin

export BASE_DIR=/mnt/SaucerBackup/OptwareStuff/TomatoUSB/

TOMATO_GIT_DIR=$(pwd)/tomato_git

if [ ! -e "/opt/brcm" ]
then
  echo "Creating /opt/brcm symlink"
  sudo ln -s $TOMATO_GIT_DIR/tools/brcm /opt/brcm
fi
