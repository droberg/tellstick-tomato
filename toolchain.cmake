set( CMAKE_SYSTEM_NAME Linux )
set( CMAKE_SYSTEM_PROCESSOR mipsel )
set( CMAKE_C_COMPILER mipsel-linux-uclibc-gcc )
set( CMAKE_CXX_COMPILER mipsel-linux-uclibc-g++ )

# where is the target environment 
set( CMAKE_FIND_ROOT_PATH /mnt/SaucerBackup/OptwareStuff/TomatoUSB/tomato_extras )


# search for programs in the build host directories
SET( CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER )
# for libraries and headers in the target directories
SET( CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY )
SET( CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY )

INCLUDE_DIRECTORIES(/mnt/SaucerBackup/OptwareStuff/TomatoUSB/tomato_extras/include)
