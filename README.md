tellstick-tomato
================

Patches, instructions and files for compiling and setting up telldus-core on Tomato USB.


Router Setup (Asus RT-N16)
===============================================================================

Tomato-USB (Shibby fixes): Tomato Firmware 1.28.0000 MIPSR2-100 K26 USB AIO


Files, se [1], and [2] 
* tomato-K26USB-1.28.RT-MIPSR2-120-AIO.trx 
* extras-mips2.tar.gz


Crosdev-toolchain
===============================================================================

Use the toolchain from the Tomato GIT-repositories. Loosely based on the guide on how to 
build and rebuild Tomato for total noobs, see [3] below as well as attached file 
setupBuildEnv.sh

Make a directory for your stuff. I will call this "$BASE_DIR", put all your downloaded 
files in this dir (see list in the end)

```bash
> BASE_DIR=$(pwd)
> git clone git://repo.or.cz/tomato.git
> mv tomato tomato_git
```

I use "orign/tomato-shibby" branch. Make a local clone, for example using

```bash
> cd tomato_git
> git checkout -b tomato-shibby-local remotes/origin/tomato-shibby
```

Before building anything do

```bash
> source $BASE_DIR/setupBuildEnv.bash
```

(Do this source cmd for all shells that you want to build something in.)

Patch the Makefile for Tomato to build extra libs

```bash
> cd $BASE_DIR/tomato_git
> patch -p1 < ../Makefile_targets.patch
```

Make at least the following libs:
* libusb
* libusb10
* iconv

```bash
> mkdir $BASE_DIR/tomato_extras
> cd $BASE_DIR/tomato_git/release/src-rt/router
> make libusb
> make libusb10
> make libusb-install
> make lbiusb10-install
> cp $BASE_DIR/tomato_git/release/src-rt/router/libusb/libusb/usb.h $BASE_DIR/tomato_extras/include/
```

```bash
> make libiconv
> make libiconv-install
> cp  $BASE_DIR/tomato_git/release/src-rt/router/libiconv/include/iconv.h $BASE_DIR/tomato_extras/include/
```

Now we need to compile some other extra-libs

* confuse

Download confuse-2.6.tar.gz

```bash
> cd $BASE_DIR/tomato_git/release/src-rt/router
> tar zxfv confuse-2.6.tar.gz
> mv confuse-2.6 confuse
> make confuse-install
```

* ftdilib

Download libftdi-0.20.tar.gz
```bash
> cd $BASE_DIR/tomato_git/release/src-rt/router
> tar zxfv libftdi-0.20.tar.gz
> mv libftdi-0.20 libftdi
> make libftdi-install
>
```

* argp

Download argp-standalone-1.3.tar.gz
```bash
> cd $BASE_DIR/tomato_git/release/src-rt/router
> tar zxfv argp-standalone-1.3.tar.gz
> mv argp-standalone-1.3 argp-standalone
> make argp-standalone
> cp argp-standalone/argp.h $BASE_DIR/tomato_extras/include
> cp argp-standalone/libargp.a $BASE_DIR/tomato_extras/lib

> mv $BASE_DIR/tomato_git/release/src-rt/router/mipsel-uclibc/install/* $BASE_DIR/tomato_extras
```


Tellstick-compile
===============================================================================

cmake and crossdev

Download and unpack telldus-core or checkout the telldus-git-tree, current version. 
Loosely follow the instructions for cross-compile with cmake at [5], see attached 
file toolchain.cmake and setupBuildEnv.sh

```bash
> wget http://download.telldus.se/TellStick/Software/telldus-core/telldus-core-2.1.2.tar.gz
> tar zxfv telldus-core-2.1.2.tar.gz
> cd telldus-core-2.1.2
> mkdir build 
> cd build
> cp $BASE_DIR/toolchain.cmake .
> patch telldus-core with attached patches: sock_cloexec.patch, CMakeFiles.txt.patch
> source setupBuildEnv.sh
> cmake-gui ..
```

In Cmake-gui:
* Select configure
* Select "Specify toolchain file for cross-compiling" and then "Next"
* Look for missing dependecies, and make sure to add correct directories in the toolchain	


```bash
> cmake ..
> make
> make DESTDIR=$BASE_DIR/telldus-install install
```

============================================================================================
Files:


* Crosstool setup with cmake: 
  - toolchain.cmake

* Router scripts: 
  - setEnv.bash
  - startTelldus.bash

* Host scripts: 
  - setupBuildEnv.sh

* Patches for teldus-core 2.1.2

  - 0001-Makefile-patches-for-building-against-Tomato-USB.patch
  - 0002-Patch-to-cope-with-large-litterals-when-compiling-fo.patch
  - 0003-Patch-for-const-char-conversion-in-case-we-are-compi.patch
  - 0004-Patch-for-missing-SOCK_CLOEXEC-in-linux-kernel-for-T.patch

* Patches for tomato-usb

  - Makefile_targets.patch

============================================================================================
Links:

- [1] http://tomato.groov.pl/
- [2] http://tomato.groov.pl/download/K26/
- [3] http://tomatousb.org/tut:how-to-build-and-rebuild-tomato-for-total-noobs#7
- [4] http://download.telldus.se/TellStick/Software/telldus-core/
- [5] http://mark4h.blogspot.se/2013/04/cross-compiling-opencv-for-openwrt.html
