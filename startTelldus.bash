#!/opt/bin/bash
BASE_DIR=/mnt/BackupDisk/OptwareStuff/TomatoUSB/
ln -s /mnt/BackupDisk/OptwareStuff/TomatoUSB/telldus-install/etc/tellstick.conf /etc/tellstick.conf
mkdir /var/state
ln -s /mnt/BackupDisk/OptwareStuff/TomatoUSB/telldus-install/var/state/telldus-core.conf /var/state/telldus-core.conf

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$BASE_DIR/tomato_extras/lib:$BASE_DIR/tomato_git/tools/brcm/K26/hndtools-mipsel-uclibc-4.2.4/lib:mnt/BackupDisk/OptwareStuff/TomatoUSB/tomato_extras/lib:$BASE_DIR/tomato_extras/libiconv/usr/lib/:$BASE_DIR/tomato_extras/libusb10/usr/lib:$BASE_DIR/tomato_extras/libusb/usr/lib:

$BASE_DIR/telldus-install/usr/local/sbin/telldusd --nodaemon
